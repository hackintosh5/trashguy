# Copyright © 2020 Hackintosh5 <copyright@hack5.dev>, Zacci <trashguy@zac.cy>
#
# Written by Hackintosh5, inspired by Zacci
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import timeit
import cProfile

from trashguy import Trashguy, TRASHGUY_LEFT, TRASHGUY_RIGHT, StrCustomLen
import trashguy

if __name__ == "__main__":
    #cProfile.run("list(Trashguy(\"a\" * 500, \"🗑\", TRASHGUY_LEFT, TRASHGUY_RIGHT, \" \"))", sort="time")
    #cProfile.run("for i in range(5000):\n    list(Trashguy(\"a\" * 15, \"🗑\", TRASHGUY_LEFT, TRASHGUY_RIGHT, \" \"))", sort="time")
    #cProfile.run("for i in range(50000):\n    list(Trashguy(\"a\", \"🗑\", TRASHGUY_LEFT, TRASHGUY_RIGHT, \" \"))", sort="time")
    #i = 15
    #string = "a" * i
    #number = 5000
    #print("plain", i, timeit.timeit(lambda: list(Trashguy(string, "🗑", TRASHGUY_LEFT, TRASHGUY_RIGHT, " ")),
    #                                number=number) * 5000 / number, sep="\t")
    for i in range(1, 16):
        string = "a" * i
        number = 5000
        print("plain", i, timeit.timeit(lambda: list(Trashguy(string, "🗑", TRASHGUY_LEFT, TRASHGUY_RIGHT, " ")), number=number) * 5000 / number, sep="\t")
        print(trashguy._calc_t.cache_info())
        print(trashguy._calc_c.cache_info())
        print(trashguy._inv_calc_c.cache_info())
    for i in range(15):
        string_emoji = [StrCustomLen(2, "👰"), StrCustomLen(2, "🧑‍🦰"), "abc", "hello", *"testing"] * i
        string = "a" * len(string_emoji)
        number = 5000 if len(string_emoji) < 20 else (2000 if len(string_emoji) < 40 else (800 if len(string_emoji) < 80 else 100))
        print("plain", len(string_emoji), timeit.timeit(lambda: list(Trashguy(string, "🗑", TRASHGUY_LEFT, TRASHGUY_RIGHT, " ")), number=number) * 5000 / number, sep="\t")
        print(trashguy._calc_t.cache_info())
        print(trashguy._calc_c.cache_info())
        print(trashguy._inv_calc_c.cache_info())
        print("emoji", len(string_emoji), timeit.timeit(lambda: list(Trashguy(string_emoji, "🗑", TRASHGUY_LEFT, TRASHGUY_RIGHT, " ")), number=number) * 5000 / number, sep="\t")
        print(trashguy._calc_t.cache_info())
        print(trashguy._calc_c.cache_info())
        print(trashguy._inv_calc_c.cache_info())
