# Copyright © 2020 Hackintosh5 <copyright@hack5.dev>, Zacci <trashguy@zac.cy>
#
# Written by Hackintosh5, inspired by Zacci
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
# THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import functools
from math import floor as _floor
try:
    from math import isqrt as _isqrt
except ImportError:
    from math import sqrt as _sqrt

    def _isqrt(n):
        return _floor(_sqrt(n))
from typing import Union, Iterable
import collections
import itertools


TRASHGUY_RIGHT = "(> ^_^)>"
TRASHGUY_LEFT = "<(^_^ <)"


class Trashguy:
    __slots__ = (
        "_spacing_c",
        "_spacing",
        "_text",
        "_glyph_can",
        "_glyph_can_len"
        "_glyph_left",
        "_glyph_left",
        "_glyph_right",
        "_glyph_space",
        "_multi_char_glyphs_table",
        "_multi_char_glyphs_i_table",
        "_multi_char_glyphs_moved_table",
        "_frame_count",
        "_get_frame",
    )

    def __init__(
            self,
            trash_items: Union[str, Iterable[str]],
            glyph_can: str,
            glyph_left: str,
            glyph_right: str,
            glyph_space: str,
            spacing=1,
            single_char_glyphs: bool = False,
            no_trash_spaces: bool = False,
    ):
        """
        Create a new Trashguy animation
        :param trash_items: The things to trash, as either an iterable of strings, or a string. In the latter case,
        each character in the string will be trashed on its own
        :param glyph_can: The text to use as the trashcan
        :param glyph_left: The text to use as the trashguy, moving left
        :param glyph_right: The text to use as the trashguy, moving right
        :param glyph_space: The text to use to padding for when an element has been trashed
        :param spacing: The number of spaces to the right of the trashguy in the first frame
        :param single_char_glyphs: An override - when set to [True], all characters in [trash_items] are assumed to be
        only one grapheme long, and are treated as such. This is always [True] when [trash_items] is a [str].
        Setting this to [True] improves performance significantly.
        :param no_trash_spaces: If set to [True], any glyph that is a space (according to its isspace() method) will not
        be trashed. Setting this to [False] improves performance significantly.
        """
        assert spacing >= 0
        spacing_c = (spacing + 1) ** 2
        self._spacing_c = spacing_c
        self._spacing = spacing
        self._glyph_can = glyph_can
        self._glyph_left = glyph_left
        self._glyph_right = glyph_right
        self._glyph_space = glyph_space
        if not (isinstance(trash_items, str) or single_char_glyphs) or no_trash_spaces:
            self._text = [str(glyph) for glyph in trash_items]
            multi_char_glyphs_table = []
            multi_char_glyphs_i_table = []
            multi_char_glyphs_moved_table = []
            i = 0
            moved = 0
            frame_count = 0
            list_append = list.append
            for glyph in trash_items:
                glyph_length = len(glyph)
                count = ((spacing + i) * 2 + 3)
                if not no_trash_spaces or not glyph.isspace():
                    base = _calc_c(i + 1, spacing, spacing_c)
                    multi_char_glyphs_i_table += range(base, base + count)
                    multi_char_glyphs_moved_table += itertools.repeat(moved, count)
                    frame_count += count
                list_append(multi_char_glyphs_table, i)
                i += glyph_length
                moved += 1
            multi_char_glyphs_i_table.append(0)
            multi_char_glyphs_moved_table.append(moved)
            self._multi_char_glyphs_table = multi_char_glyphs_table + [0, 0]
            self._multi_char_glyphs_i_table = multi_char_glyphs_i_table
            self._multi_char_glyphs_moved_table = multi_char_glyphs_moved_table
            self._frame_count = frame_count + 1
            self._get_frame = self._get_frame_mcg
        else:
            self._text = trash_items
            self._frame_count = _floor(_calc_c(len(self._text) + 1, spacing, spacing_c)) + 1
            self._get_frame = self._get_frame_scg

    def _get_frame_scg(self, i):
        spacing = self._spacing
        spacing_c = self._spacing_c
        inv_c_i_cache = _inv_calc_c(i, spacing, spacing_c)
        o_cache = _calc_c(inv_c_i_cache, spacing, spacing_c)
        m_cache = _calc_c(inv_c_i_cache + 1, spacing, spacing_c)
        t_cache = _calc_t(o_cache, m_cache)
        return (
            self._get_frame_moveright_scg(i - o_cache, inv_c_i_cache - 1)
            if i < t_cache else
            self._get_frame_moveleft_scg(m_cache - i - 1, inv_c_i_cache - 1)
        )

    def _get_frame_mcg(self, i):
        moved = self._multi_char_glyphs_moved_table[i]
        i = self._multi_char_glyphs_i_table[i]
        spacing = self._spacing
        spacing_c = self._spacing_c
        inv_c_i_cache = _inv_calc_c(i, spacing, spacing_c)
        o_cache = _calc_c(inv_c_i_cache, spacing, spacing_c)
        m_cache = _calc_c(inv_c_i_cache + 1, spacing, spacing_c)
        t_cache = _calc_t(o_cache, m_cache)
        return (
            self._get_frame_moveright_mcg(i - o_cache, moved)
            if i < t_cache else
            self._get_frame_moveleft_mcg(m_cache - i - 1, moved)
        )

    def _get_frame_moveleft_scg(self, pos, moved):
        if pos:
            return f"{self._glyph_can}" \
                   f"{self._glyph_space * (pos - 1)}" \
                   f"{self._text[moved]}" \
                   f"{self._glyph_left}" \
                   f"{self._glyph_space * (moved - pos + self._spacing + 1)}" \
                   f"{self._text[moved + 1:]}"
        else:
            return f"{self._glyph_can}" \
                   f"{self._glyph_left}" \
                   f"{self._glyph_space * (moved + self._spacing + 1)}" \
                   f"{self._text[moved + 1:]}"

    def _get_frame_moveleft_mcg(self, pos, moved):
        if pos:
            return f"{self._glyph_can}" \
                   f"{self._glyph_space * (pos - 1)}" \
                   f"{self._text[moved]}" \
                   f"{self._glyph_left}" \
                   f"{self._glyph_space * (self._multi_char_glyphs_table[moved] - pos + self._spacing + 1)}" \
                   f"{''.join(self._text[moved + 1:])}"
        else:
            return f"{self._glyph_can}" \
                   f"{self._glyph_left}" \
                   f"{self._glyph_space * (self._multi_char_glyphs_table[moved + 1] + self._spacing)}" \
                   f"{''.join(self._text[moved + 1:])}"

    def _get_frame_moveright_scg(self, pos, moved):
        return f"{self._glyph_can}" \
               f"{self._glyph_space * pos}" \
               f"{self._glyph_right}" \
               f"{self._glyph_space * (moved + self._spacing - pos)}" \
               f"{self._text[moved:]}"

    def _get_frame_moveright_mcg(self, pos, moved):
        return f"{self._glyph_can}" \
               f"{self._glyph_space * pos}" \
               f"{self._glyph_right}" \
               f"{self._glyph_space * (self._multi_char_glyphs_table[moved] + self._spacing - pos)}" \
               f"{''.join(self._text[moved:])}"

    def __iter__(self):
        for i in range(self._frame_count):
            yield self._get_frame(i)

    def __getitem__(self, item):
        if isinstance(item, slice):
            get_frame = self._get_frame
            return (get_frame(i) for i in item.indices(self._frame_count))
        if (not __debug__) or isinstance(item, int):
            return self._get_frame(item)

        raise TypeError(f"indices must be slices or integers, not {type(item)}")

    def __len__(self):
        return self._frame_count

    def __str__(self):
        return "\n".join(self)


@functools.lru_cache
def _calc_t(o, m):
    return (o + m) // 2


@functools.lru_cache
def _calc_c(j, spacing, spacing_c):
    return (j + spacing) ** 2 - spacing_c


@functools.lru_cache(None)
def _inv_calc_c(j, spacing, spacing_c):
    return _isqrt(j + spacing_c) - spacing


class StrCustomLen(collections.UserString):
    """
    Class to wrap a string but providing a custom length
    Used to count how many spaces to use when the element is trashed
    """
    length: int

    def __init__(self, length, *args, **kwargs):
        self.length = length
        super().__init__(*args, **kwargs)

    def __len__(self):
        return self.length


if __name__ == "__main__":
    print(Trashguy("hello world", "🗑", TRASHGUY_LEFT, TRASHGUY_RIGHT, " ", spacing=5))
    print(Trashguy([StrCustomLen(2, "👰"), StrCustomLen(2, "🧑‍🦰"), "abc", " ", "hello", *"testing"], "🗑", TRASHGUY_LEFT, TRASHGUY_RIGHT, " ", spacing=5, no_trash_spaces=True))
